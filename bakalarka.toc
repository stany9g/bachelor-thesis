\babel@toc {czech}{}
\babel@toc {czech}{}
\babel@toc {czech}{}
\babel@toc {czech}{}
\babel@toc {czech}{}
\babel@toc {czech}{}
\contentsline {paragraph}{Kl\'i\v cov\'a\ slova:}{vi}{paragraph*.1}
\contentsline {paragraph}{Vedouc\IeC {\'\i }:}{vi}{paragraph*.2}
\babel@toc {english}{}
\contentsline {paragraph}{Keywords:}{vi}{paragraph*.3}
\contentsline {paragraph}{Title\ translation:}{vi}{paragraph*.4}
\babel@toc {czech}{}
\babel@toc {czech}{}
\babel@toc {czech}{}
\babel@toc {english}{}
\babel@toc {czech}{}
\babel@toc {czech}{}
\babel@toc {czech}{}
\babel@toc {czech}{}
\contentsline {chapter}{\numberline {1}\IeC {\'U}vod}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Motivace}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}C\IeC {\'\i }le}{2}{section.1.2}
\contentsline {chapter}{\numberline {2}Popis probl\IeC {\'e}mu}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Rozd\IeC {\v e}len\IeC {\'\i } dle metody sb\IeC {\v e}ru dat}{3}{section.2.1}
\contentsline {section}{\numberline {2.2}Definice dotazn\IeC {\'\i }ku}{3}{section.2.2}
\contentsline {section}{\numberline {2.3}Inteligentn\IeC {\'\i } dotazn\IeC {\'\i }k}{4}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Dotazov\IeC {\'a}n\IeC {\'\i }}{4}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Probl\IeC {\'e}my spojen\IeC {\'e} s~vypl\IeC {\v n}ov\IeC {\'a}n\IeC {\'\i }m dotazn\IeC {\'\i }k\IeC {\r u}}{4}{subsection.2.3.2}
\contentsline {section}{\numberline {2.4}Struktura dotazn\IeC {\'\i }ku}{5}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}\IeC {\'U}vod}{5}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Ot\IeC {\'a}zky}{5}{subsection.2.4.2}
\contentsline {section}{\numberline {2.5}Dal\IeC {\v s}\IeC {\'\i } pro n\IeC {\'a}s d\IeC {\r u}le\IeC {\v z}it\IeC {\'e} rozd\IeC {\v e}len\IeC {\'\i }}{5}{section.2.5}
\contentsline {section}{\numberline {2.6}Vyu\IeC {\v z}it\IeC {\'\i } test\IeC {\r u} a pr\IeC {\r u}zkum\IeC {\r u}}{6}{section.2.6}
\contentsline {subsection}{\numberline {2.6.1}Testy}{6}{subsection.2.6.1}
\contentsline {subsection}{\numberline {2.6.2}Pr\IeC {\r u}zkumy}{6}{subsection.2.6.2}
\contentsline {section}{\numberline {2.7}P\IeC {\v r}\IeC {\'\i }klady vyu\IeC {\v z}it\IeC {\'\i }}{7}{section.2.7}
\contentsline {subsection}{\numberline {2.7.1}SUS: System usability scale}{7}{subsection.2.7.1}
\contentsline {subsection}{\numberline {2.7.2}Belbin\IeC {\r u}v test rol\IeC {\'\i }}{8}{subsection.2.7.2}
\contentsline {section}{\numberline {2.8}Existuj\IeC {\'\i }c\IeC {\'\i } programy}{9}{section.2.8}
\contentsline {subsection}{\numberline {2.8.1}Prvn\IeC {\'\i } kolo hled\IeC {\'a}n\IeC {\'\i }}{10}{subsection.2.8.1}
\contentsline {subsection}{\numberline {2.8.2}Survio}{11}{subsection.2.8.2}
\contentsline {subsubsection}{V\IeC {\'y}hody}{11}{figure.2.3}
\contentsline {subsubsection}{Nev\IeC {\'y}hody}{11}{figure.2.3}
\contentsline {subsection}{\numberline {2.8.3}Click4Survey}{12}{subsection.2.8.3}
\contentsline {subsubsection}{V\IeC {\'y}hody}{12}{figure.2.4}
\contentsline {subsubsection}{Nev\IeC {\'y}hody}{12}{figure.2.4}
\contentsline {subsection}{\numberline {2.8.4}Druh\IeC {\'e} kolo hled\IeC {\'a}n\IeC {\'\i }}{13}{subsection.2.8.4}
\contentsline {subsection}{\numberline {2.8.5}Typeform}{14}{subsection.2.8.5}
\contentsline {subsubsection}{V\IeC {\'y}hody}{14}{figure.2.6}
\contentsline {subsubsection}{Nev\IeC {\'y}hody}{14}{figure.2.6}
\contentsline {subsection}{\numberline {2.8.6}Enalyzer}{15}{subsection.2.8.6}
\contentsline {subsubsection}{V\IeC {\'y}hody}{15}{figure.2.7}
\contentsline {subsubsection}{Nev\IeC {\'y}hody}{16}{figure.2.7}
\contentsline {subsection}{\numberline {2.8.7}Formul\IeC {\'a}\IeC {\v r}e Google}{16}{subsection.2.8.7}
\contentsline {subsubsection}{V\IeC {\'y}hody}{16}{figure.2.8}
\contentsline {subsubsection}{Nev\IeC {\'y}hody}{17}{figure.2.8}
\contentsline {section}{\numberline {2.9}Shrnut\IeC {\'\i }}{17}{section.2.9}
\contentsline {chapter}{\numberline {3}Anal\IeC {\'y}za a n\IeC {\'a}vrh syst\IeC {\'e}mu}{19}{chapter.3}
\contentsline {section}{\numberline {3.1}Po\IeC {\v z}adavky}{19}{section.3.1}
\contentsline {section}{\numberline {3.2}Funk\IeC {\v c}n\IeC {\'\i } po\IeC {\v z}adavky}{20}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Spr\IeC {\'a}va dotazn\IeC {\'\i }ku}{20}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Vypln\IeC {\v e}n\IeC {\'\i } dotazn\IeC {\'\i }ku}{20}{subsection.3.2.2}
\contentsline {section}{\numberline {3.3}Nefunk\IeC {\v c}n\IeC {\'\i } po\IeC {\v z}adavky}{21}{section.3.3}
\contentsline {section}{\numberline {3.4}P\IeC {\v r}\IeC {\'\i }pady u\IeC {\v z}it\IeC {\'\i }}{21}{section.3.4}
\contentsline {section}{\numberline {3.5}Sc\IeC {\'e}n\IeC {\'a}\IeC {\v r}e p\IeC {\v r}\IeC {\'\i }pad\IeC {\r u} u\IeC {\v z}it\IeC {\'\i }}{22}{section.3.5}
\contentsline {subsubsection}{FR01.1 Vytvo\IeC {\v r}en\IeC {\'\i } dotazn\IeC {\'\i }ku}{22}{section.3.5}
\contentsline {subsubsection}{FR01.2 \IeC {\'U}prava dotazn\IeC {\'\i }ku}{22}{Item.3}
\contentsline {subsubsection}{FR01.3 Smaz\IeC {\'a}n\IeC {\'\i } dotazn\IeC {\'\i }ku}{23}{Item.6}
\contentsline {subsubsection}{FR02.1 P\IeC {\v r}idat ot\IeC {\'a}zku}{23}{Item.12}
\contentsline {subsubsection}{FR02.2 Upravit ot\IeC {\'a}zku}{24}{Item.16}
\contentsline {subsubsection}{FR02.3 Odstranit ot\IeC {\'a}zku}{24}{Item.19}
\contentsline {subsubsection}{FR03.1 P\IeC {\v r}idat odpov\IeC {\v e}\IeC {\v d}}{24}{Item.25}
\contentsline {subsubsection}{FR03.2 Upravit odpov\IeC {\v e}\IeC {\v d}}{25}{Item.31}
\contentsline {subsubsection}{FR03.3 Odstranit odpov\IeC {\v e}\IeC {\v d}}{25}{Item.34}
\contentsline {subsubsection}{FR04 Logika pr\IeC {\r u}chodu}{25}{Item.40}
\contentsline {subsubsection}{FR05.1 Zobrazit v\IeC {\'y}sledky}{26}{Item.43}
\contentsline {subsubsection}{FR05.2 Export v\IeC {\'y}sledk\IeC {\r u}}{26}{Item.45}
\contentsline {subsubsection}{FR06 Na\IeC {\v c}ten\IeC {\'\i } dotazn\IeC {\'\i }ku}{26}{Item.48}
\contentsline {subsubsection}{FR07 Ulo\IeC {\v z}en\IeC {\'\i } dotazn\IeC {\'\i }ku dotazn\IeC {\'\i }ku}{26}{Item.53}
\contentsline {subsubsection}{FR08 Zobrazen\IeC {\'\i } dotazn\IeC {\'\i }k\IeC {\r u}}{27}{Item.60}
\contentsline {subsubsection}{FR09.1 Vypln\IeC {\v e}n\IeC {\'\i } dotazn\IeC {\'\i }ku}{27}{Item.62}
\contentsline {subsubsection}{FR09.2 Odpov\IeC {\v e}\IeC {\v d} na ot\IeC {\'a}zku}{27}{Item.64}
\contentsline {subsubsection}{FR10 Odeslat k~vyhodnocen\IeC {\'\i }}{27}{Item.67}
\contentsline {subsubsection}{FR11 Ulo\IeC {\v z}en\IeC {\'\i } v\IeC {\'y}sledk\IeC {\r u}}{28}{Item.74}
\contentsline {section}{\numberline {3.6}Diagram t\IeC {\v r}\IeC {\'\i }d}{28}{section.3.6}
\contentsline {subsection}{\numberline {3.6.1}Popis diagramu t\IeC {\v r}\IeC {\'\i }d}{29}{subsection.3.6.1}
\contentsline {section}{\numberline {3.7}Soubor dotazn\IeC {\'\i }ku}{31}{section.3.7}
\contentsline {subsection}{\numberline {3.7.1}Popis XML (Extensible Markup language)}{31}{subsection.3.7.1}
\contentsline {subsubsection}{P\IeC {\v r}\IeC {\'\i }klad XML form\IeC {\'a}tu}{31}{subsubsection*.5}
\contentsline {subsubsection}{P\IeC {\v r}\IeC {\'\i }klad XML form\IeC {\'a}tu}{31}{subsubsection*.5}
\contentsline {subsection}{\numberline {3.7.2}Popis JSON (JavaScript Object Notation)}{31}{subsection.3.7.2}
\contentsline {subsubsection}{P\IeC {\v r}\IeC {\'\i }klad form\IeC {\'a}tu}{31}{subsubsection*.6}
\contentsline {subsubsection}{P\IeC {\v r}\IeC {\'\i }klad form\IeC {\'a}tu}{32}{subsubsection*.6}
\contentsline {subsection}{\numberline {3.7.3}Volba form\IeC {\'a}tu}{32}{subsection.3.7.3}
\contentsline {subsection}{\numberline {3.7.4}Zp\IeC {\r u}sob ukl\IeC {\'a}d\IeC {\'a}n\IeC {\'\i }}{32}{subsection.3.7.4}
\contentsline {subsection}{\numberline {3.7.5}Popis AES (Advanced Encryption Standard)}{32}{subsection.3.7.5}
\contentsline {section}{\numberline {3.8}Shrnut\IeC {\'\i } anal\IeC {\'y}zy a n\IeC {\'a}vrhu syst\IeC {\'e}mu}{33}{section.3.8}
\contentsline {chapter}{\numberline {4}Implementace}{35}{chapter.4}
\contentsline {section}{\numberline {4.1}Pou\IeC {\v z}it\IeC {\'a} technologie}{35}{section.4.1}
\contentsline {section}{\numberline {4.2}Struktura aplikace}{35}{section.4.2}
\contentsline {section}{\numberline {4.3}Probl\IeC {\'e}my p\IeC {\v r}i implementaci}{36}{section.4.3}
\contentsline {chapter}{\numberline {5}U\IeC {\v z}ivatelsk\IeC {\'e} testov\IeC {\'a}n\IeC {\'\i }}{37}{chapter.5}
\contentsline {section}{\numberline {5.1}Popis testov\IeC {\'a}n\IeC {\'\i }}{37}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Pre-test dotazn\IeC {\'\i }k}{37}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Post-test dotazn\IeC {\'\i }k}{38}{subsection.5.1.2}
\contentsline {section}{\numberline {5.2}Testov\IeC {\'a}n\IeC {\'\i } s~u\IeC {\v z}ivateli}{38}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Participant 1}{38}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Participant 2}{38}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Participant 3}{38}{subsection.5.2.3}
\contentsline {subsection}{\numberline {5.2.4}Participant 4}{39}{subsection.5.2.4}
\contentsline {subsection}{\numberline {5.2.5}Participant 5}{39}{subsection.5.2.5}
\contentsline {section}{\numberline {5.3}Vyhodnocen\IeC {\'\i } v\IeC {\'y}sledk\IeC {\r u} testov\IeC {\'a}n\IeC {\'\i }}{39}{section.5.3}
\contentsline {chapter}{\numberline {6}Z\IeC {\'a}v\IeC {\v e}r}{41}{chapter.6}
\contentsline {section}{\numberline {6.1}Zhodnocen\IeC {\'\i }}{41}{section.6.1}
\contentsline {chapter}{\numberline {A}Literatura}{43}{appendix.A}
\contentsline {chapter}{\numberline {B}U\IeC {\v z}ivatelsk\IeC {\'y} manu\IeC {\'a}l}{45}{appendix.B}
\contentsline {section}{\IeC {\'U}vod}{45}{section*.7}
\contentsline {section}{Instalace aplikace}{45}{section*.7}
\contentsline {section}{Popis aplikace}{46}{section*.7}
\contentsline {section}{Jak pracovat se soubory dotazn\IeC {\'\i }ku}{57}{figure.B.9}
\contentsline {chapter}{\numberline {C}Ot\IeC {\'a}zky pro participanty testov\IeC {\'a}n\IeC {\'\i }}{59}{appendix.C}
\contentsline {chapter}{\numberline {D}V\IeC {\'y}sledky testu pou\IeC {\v z}itelnosti}{61}{appendix.D}
